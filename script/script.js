var App = (function (axios, Mustache) {
	var genres = [];
	var movies = [];
	//var wind = [];

	var $genres = document.getElementById('genres');
	var $movies = document.getElementById('movies');
	//var $prozor1 = document.getElementById('prozor1');
	var genresTpl = document.getElementById('genres-template').innerHTML;
	var moviesTemplate = document.getElementById('movies-template').innerHTML;
	var prozor1Template = document.getElementById('prozor1-template').innerHTML;

	return {
		getCurrentGenre: function () {
		},
		getMoviesByGenre: function () {
		},
		start: function () {
			var self = this;

			this.attachEvents();
			this.fetch()
			.then(function (response) {
				var data = response.data;
				genres = data.genres;
				movies = data.movies;
				//prozor1 = data.prozor1;

				self.createLinks();
			});
		},
		fetch: function (cb) {
			return axios.get('db.json');
		},
		createLinks: function () {
			$genres.innerHTML = Mustache.render(genresTpl, {
				genres: genres
			});
		},
		updateMovies: function () {
			var x = window.location.hash;
			x = x.slice(2);
			console.log(x)
			var a = [];
			for (let i=0; i<movies.length; i++){
				for (let j=0; j<movies[i].genres.length; j++) {
					if(movies[i].genres[j] == x) {
						a.push(movies[i])
					}
				}
			}
			
			// TODO: compile movies template;
			$movies.innerHTML = Mustache.render(moviesTemplate, {
				movies: a
			});
		},
		
	

		attachEvents: function () {
			window.addEventListener('hashchange', this.updateMovies.bind(this));
		}
	}
})(axios, Mustache);




